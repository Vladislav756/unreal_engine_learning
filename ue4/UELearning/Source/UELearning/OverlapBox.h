#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "OverlapBox.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTriggerActivated, bool, bIsOverlapped);

UCLASS()
class UELEARNING_API AOverlapBox : public AActor
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& hitResult);

	UFUNCTION()
	void OverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	AOverlapBox();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* OverlapBox;

	UPROPERTY(BlueprintAssignable)
	FOnTriggerActivated OnTriggerActivated;

protected:
	virtual void BeginPlay() override;
};

