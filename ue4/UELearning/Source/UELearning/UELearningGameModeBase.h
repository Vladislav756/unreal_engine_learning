// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UELearningGameModeBase.generated.h"

UCLASS()
class UELEARNING_API AUELearningGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
