#pragma once

#define ECC_Bullet ECC_GameTraceChannel3

const FName FXParamTraceEnd = FName("TraceEnd");

const FName CollisionRagdollProfile = FName("Ragdoll");

const FName SocketCharacterWeapon = FName("CharacterWeaponSocket");
const FName SocketCharacterBow = FName("CharacterBowSocket"); //this was added
const FName SocketWeaponMuzzle = FName("MuzzleSocket");
const FName SocketWeaponForeGrip = FName("ForeGripSocket");
const FName SocketArrowSpawn = FName("ArrowSpawnSocket");

const FName DebugCategoryRangeWeapon = FName("RangeWeapon");

UENUM(BlueprintType)
enum class EEquipableItemType : uint8
{
	None,
	Pistol,
	Rifle,
	Bow
};