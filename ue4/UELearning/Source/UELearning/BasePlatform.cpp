#include "BasePlatform.h"
#include "OverlapBox.h"

ABasePlatform::ABasePlatform()
{
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* DefaultPlatformRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Platform root"));
	RootComponent = DefaultPlatformRoot;

	PlatformMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform"));
	PlatformMesh->SetupAttachment(DefaultPlatformRoot);
}

void ABasePlatform::PlatformTimelineUpdate(const float Alpha)
{
	const FVector PlatformTargetLocation = FMath::Lerp(StartLocation, EndLocation, Alpha);
	PlatformMesh->SetRelativeLocation(PlatformTargetLocation);

}

void ABasePlatform::BeginPlay()
{
	Super::BeginPlay();
	StartLocation = PlatformMesh->GetRelativeLocation();

	if (IsValid(TimelineCurve))
	{
		FOnTimelineFloatStatic PlatformMovementTimelineUpdate;
		PlatformMovementTimelineUpdate.BindUObject(this, &ABasePlatform::PlatformTimelineUpdate);
		PlatformTimeline.AddInterpFloat(TimelineCurve, PlatformMovementTimelineUpdate);

		if (PlatformBehavior == EPlatformBehavior::Loop) // Loop platform
		{
			FOnTimelineEventStatic OnTimelineFinished;
			OnTimelineFinished.BindUObject(this, &ABasePlatform::OnEndTimeline);
			PlatformTimeline.SetTimelineFinishedFunc(OnTimelineFinished);
			PlatformTimeline.PlayFromStart();
		}

		if (IsValid(OverlapBox) && PlatformBehavior == EPlatformBehavior::OnDemand)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Overlapwork"));

			OverlapBox->OnTriggerActivated.AddDynamic(this, &ABasePlatform::OnBoxTriggered);
		}
	}
}

void ABasePlatform::OnBoxTriggered(bool bIsOverlapped)
{
	if (bIsOverlapped)
	{
		PlatformTimeline.Play();
	}
	else
	{
		//ABasePlatform::OnDemandPause();

		PlatformTimeline.Reverse();
	}
}

void ABasePlatform::OnEndTimeline()
{
	float CurrentPosition = PlatformTimeline.GetPlaybackPosition();

	if (FMath::IsNearlyEqual(CurrentPosition, 0.0f, 1e-6f))
	{
		PlatformTimeline.PlayFromStart();
	}
	else 
	{
		PlatformTimeline.ReverseFromEnd();
	}
}

void ABasePlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	PlatformTimeline.TickTimeline(DeltaTime);
}