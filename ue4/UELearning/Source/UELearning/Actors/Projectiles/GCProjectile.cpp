#include "GCProjectile.h"

#include "Actors/Equipment/Weapons/RangeWeaponItem.h" //this one was added
#include "Characters/GCBaseCharacter.h" //this one was added
#include "Components/SphereComponent.h"
#include "GameCodeTypes.h" //this one was added
#include "GameFramework/ProjectileMovementComponent.h"
#include "Actors/Equipment/Weapons/BowWeaponItem.h"

AGCProjectile::AGCProjectile()
{
	ArrowMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ArrowMesh"));
	ArrowMesh->SetupAttachment(RootComponent, SocketArrowSpawn);

	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	CollisionComponent->InitSphereRadius(5.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Block);
	CollisionComponent->OnComponentHit.AddDynamic(this, &AGCProjectile::ProjectileHit);
	SetRootComponent(CollisionComponent);
	
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComponent"));
	
	//ProjectileMovementComponent->InitialSpeed;
	//ProjectileMovementComponent->MaxSpeed;
}

void AGCProjectile::LaunchProjectile(FVector Direction, float InitialProjectileSpeed)
{
	ProjectileMovementComponent->Velocity = Direction * InitialProjectileSpeed; //ProjectileMovementComponent->InitialSpeed;
	CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true);
}

//this one was added
void AGCProjectile::ProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector normalImpulse, const FHitResult& Hit)
{	
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("Projectile hit %s"), *OtherActor->GetName()));

	if (OtherActor && OtherActor != this && OtherComp)
	{
		FHitResult HitResult;
		
		AGCBaseCharacter* CharacterOwner = Cast<AGCBaseCharacter>(GetOwner());

		AController* Controller = CharacterOwner->GetController();

		OtherActor->TakeDamage(ProjectileDamage, FDamageEvent{}, Controller, this);

		Destroy();
	}
}