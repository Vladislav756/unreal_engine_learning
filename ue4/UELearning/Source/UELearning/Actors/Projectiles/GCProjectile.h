#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GCProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UCurveFloat;
class UPrimitiveComponent; //this one was added

UCLASS()
class UELEARNING_API AGCProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AGCProjectile();

	UPROPERTY(VisibleAnywhere, Category = "Arrow")
	UStaticMeshComponent* ArrowMesh;

	UFUNCTION (BlueprintCallable)
	void LaunchProjectile(FVector Direction, float InitialProjectileSpeed);

protected:

	//here can be mesh destroy time probably? It appears in UE, should it add here?

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USphereComponent* CollisionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UProjectileMovementComponent* ProjectileMovementComponent;
	 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Parameters")
	float ProjectileDamage = 50.0f;

	UFUNCTION()
	void ProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector normalImpulse, const FHitResult& Hit); //this one was added
};
