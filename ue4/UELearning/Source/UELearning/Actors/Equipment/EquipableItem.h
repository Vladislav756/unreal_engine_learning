#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameCodeTypes.h"
#include "EquipableItem.generated.h"

UCLASS(Abstract, NotBlueprintable)
class UELEARNING_API AEquipableItem : public AActor
{
	GENERATED_BODY()

public:
	EEquipableItemType GetItemType() const { return ItemType; };

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Equippable Item")
	EEquipableItemType ItemType = EEquipableItemType::None;
};
