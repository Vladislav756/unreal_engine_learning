#include "MachineryWeaponItem.h"

#include "Components/Weapon/WeaponBarellComponent.h"
#include "Characters/GCBaseCharacter.h"


void AMachineryWeaponItem::OnWeaponStartFire()
{
	WeaponShot();
	if (WeaponFireMode == EWeaponFireMode::FullAuto)
	{
		GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
		GetWorld()->GetTimerManager().SetTimer(ShotTimer, this, &AMachineryWeaponItem::WeaponShot, GetShotTimerInterval(), true);
	}
}

void AMachineryWeaponItem::OnWeaponStopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
}

FTransform AMachineryWeaponItem::GetForeGripTransform() const
{
	return WeaponMesh->GetSocketTransform(SocketWeaponForeGrip);
}

void AMachineryWeaponItem::WeaponShot()
{
	checkf(GetOwner()->IsA<AGCBaseCharacter>(), TEXT("AMachineryWeaponItem::WeaponShot() only character can be an owner of range weapon"));
	AGCBaseCharacter* CharacterOwner = StaticCast<AGCBaseCharacter*>(GetOwner());

	APlayerController* Controller = CharacterOwner->GetController<APlayerController>();
	if (!IsValid(Controller))
	{
		return;
	}

	FVector PlayerViewPoint;
	FRotator PlayerViewRotation;

	Controller->GetPlayerViewPoint(PlayerViewPoint, PlayerViewRotation);

	FVector ViewDirection = PlayerViewRotation.RotateVector(FVector::ForwardVector);
	ViewDirection += GetBulletSpreadOffset(FMath::RandRange(0.0f, GetCurrentBulletSpreadAngle()), PlayerViewRotation) /*const*/;  //this for rifle bullet spread

	WeaponBarell->Shot(PlayerViewPoint, ViewDirection, Controller);

	CharacterOwner->PlayAnimMontage(CharacterFireMontage);
	PlayAnimMontage(WeaponFireMontage);
}

FVector AMachineryWeaponItem::GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const
{
	float SpreadSize = FMath::Tan(Angle);
	float RotationAngle = FMath::RandRange(0.0f, 2 * PI);

	float SpreadY = FMath::Cos(RotationAngle);
	float SpreadZ = FMath::Sin(RotationAngle);

	FVector Result = (ShotRotation.RotateVector(FVector::UpVector) * SpreadZ + ShotRotation.RotateVector(FVector::RightVector) * SpreadY) * SpreadSize;

	return Result;
}

float AMachineryWeaponItem::GetCurrentBulletSpreadAngle() const
{
	float AngleInDegress = bIsAiming ? AimSpreadAngle : SpreadAngle;

	return FMath::DegreesToRadians(AngleInDegress);
}