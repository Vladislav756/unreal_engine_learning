#pragma once

#include "CoreMinimal.h"
#include "Actors/Equipment/Weapons/RangeWeaponItem.h"

#include "MachineryWeaponItem.generated.h"

UCLASS()
class UELEARNING_API AMachineryWeaponItem : public ARangeWeaponItem
{
	GENERATED_BODY()
	
public:

	virtual void OnWeaponStartFire() override;

	virtual void OnWeaponStopFire() override;

	FTransform GetForeGripTransform() const;

protected:

	virtual void WeaponShot();

	// Rate of Fire in Rounds per minute
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parameters, meta = (ClampMin = 1.0f, UIMin = 1.0f)")
	float RateOfFire = 600.0f;

private:
	virtual float GetCurrentBulletSpreadAngle() const override;

	float GetShotTimerInterval() const { return 60.0f / RateOfFire; };

	virtual FVector GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const override;

	FTimerHandle ShotTimer;
};
