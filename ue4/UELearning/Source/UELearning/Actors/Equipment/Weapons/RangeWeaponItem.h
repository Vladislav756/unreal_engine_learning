#pragma once

#include "CoreMinimal.h"
#include "Actors/Equipment/EquipableItem.h"
#include "Actors/Projectiles/GCProjectile.h"
#include "RangeWeaponItem.generated.h"

class USkeletalMeshComponent;
class UWeaponBarellComponent;

UENUM (BlueprintType)
enum class EWeaponFireMode : uint8
{
	Single,
	FullAuto,
	Arrow
};

UCLASS(Blueprintable)
class UELEARNING_API ARangeWeaponItem : public AEquipableItem
{
	GENERATED_BODY()
	
public:

	ARangeWeaponItem();

	/*void*/ bool StartAim() { return bIsAiming = true; }			

	/*void*/ bool StopAim() { return bIsAiming = false; }

	float GetAimFOV() const { return AimFOV; }

	float GetAimMovementMaxSpeed() const { return AimMovementMaxSpeed; }

	virtual void OnWeaponStartFire();

	virtual void OnWeaponStopFire();

	virtual void WeaponShot();

	bool bIsAiming;

protected:

	float PlayAnimMontage(UAnimMontage* Montage);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class UWeaponBarellComponent* WeaponBarell;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | Weapon ")
	UAnimMontage* WeaponFireMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | Character ")
	UAnimMontage* CharacterFireMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parameters") //, meta = (ClampMin = 0.0f, UIMin = 0.0f, ClampMin = 2.0f, UIMax = 2.0f)")
	float AimMovementMaxSpeed = 200.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parameters") //, meta = (ClampMin = 0.0f, UIMin = 0.0f, ClampMin = 2.0f, UIMax = 2.0f)")
	float AimFOV = 60.0f;

	// Bullet Spread Half angle in degrees
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parameters, meta = (ClampMin = 0.0f, UIMin = 0.0f, ClampMin = 2.0f, UIMax = 2.0f)")
	float SpreadAngle = 1.0f;

	// Bullet Spread Half angle in degrees
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parameters | Aiming, meta = (ClampMin = 0.0f, UIMin = 0.0f, ClampMin = 2.0f, UIMax = 2.0f)")
	float AimSpreadAngle = 0.25f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parameters")
	EWeaponFireMode WeaponFireMode = EWeaponFireMode::Single;

	virtual FVector GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const;

	virtual float GetCurrentBulletSpreadAngle() const;
};