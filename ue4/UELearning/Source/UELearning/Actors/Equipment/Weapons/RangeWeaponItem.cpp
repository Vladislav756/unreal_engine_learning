#include "RangeWeaponItem.h"

#include "Actors/Projectiles/GCProjectile.h" //this one was added
#include "Characters/GCBaseCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "Components/Weapon/WeaponBarellComponent.h"  //this one was added
#include "GameCodeTypes.h"
#include "GameFramework/Character.h"
#include "MachineryWeaponItem.h" //this one was added

ARangeWeaponItem::ARangeWeaponItem()
{
	RootComponent = CreateDefaultSubobject <USceneComponent>(TEXT("WeaponRoot"));

	WeaponMesh = CreateDefaultSubobject <USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(RootComponent);

	WeaponBarell = CreateDefaultSubobject<UWeaponBarellComponent>(TEXT("WeaponBarell"));
	WeaponBarell->SetupAttachment(WeaponMesh, SocketWeaponMuzzle);
}

void ARangeWeaponItem::WeaponShot()
{
	checkf(GetOwner()->IsA<AGCBaseCharacter>(), TEXT("ARangeWeaponItem::Fire() only character can be an owner of range weapon"));
	AGCBaseCharacter* CharacterOwner = StaticCast<AGCBaseCharacter*>(GetOwner());

	APlayerController* Controller = CharacterOwner->GetController<APlayerController>();
	if (!IsValid(Controller))
	{
		return;
	}

	FVector PlayerViewPoint;
	FRotator PlayerViewRotation;

	Controller->GetPlayerViewPoint(PlayerViewPoint, PlayerViewRotation);

	FVector ViewDirection = PlayerViewRotation.RotateVector(FVector::ForwardVector);
	ViewDirection += GetBulletSpreadOffset(FMath::RandRange(0.0f, GetCurrentBulletSpreadAngle()), PlayerViewRotation) /*const*/;  //this for rifle bullet spread

	WeaponBarell->Shot(PlayerViewPoint, ViewDirection, Controller);

	CharacterOwner->PlayAnimMontage(CharacterFireMontage);
	PlayAnimMontage(WeaponFireMontage);
}

void ARangeWeaponItem::OnWeaponStartFire()
{
	
}

void ARangeWeaponItem::OnWeaponStopFire()
{
	
}

float ARangeWeaponItem::PlayAnimMontage(UAnimMontage* Montage)
{
	UAnimInstance* AnimInstance = WeaponMesh->GetAnimInstance();
	float Result = 0.0f;
	if (IsValid(AnimInstance))
	{
		Result = AnimInstance->Montage_Play(Montage);
	}
	return Result;
}

FVector ARangeWeaponItem::GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const
{
	float SpreadSize = FMath::Tan(Angle);
	float RotationAngle = FMath::RandRange(0.0f, 2 * PI);

	float SpreadY = FMath::Cos(RotationAngle);
	float SpreadZ = FMath::Sin(RotationAngle);

	FVector Result = (ShotRotation.RotateVector(FVector::UpVector) * SpreadZ + ShotRotation.RotateVector(FVector::RightVector) * SpreadY) * SpreadSize;

	return Result;
}

float ARangeWeaponItem::GetCurrentBulletSpreadAngle() const
{
	float AngleInDegress = bIsAiming ? AimSpreadAngle : SpreadAngle;

	return FMath::DegreesToRadians(AngleInDegress);
}