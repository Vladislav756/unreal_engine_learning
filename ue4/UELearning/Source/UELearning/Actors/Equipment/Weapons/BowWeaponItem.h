#pragma once

#include "CoreMinimal.h"
#include "Actors/Equipment/Weapons/RangeWeaponItem.h"
#include "Components/TimelineComponent.h"
#include "BowWeaponItem.generated.h"

class AGCProjectile;

UCLASS()
class UELEARNING_API ABowWeaponItem : public ARangeWeaponItem
{
	GENERATED_BODY()

public:

	ABowWeaponItem();

	FTransform GetForeGripTransform() const;

	bool GetIsTension() const { return bIsTension; }

	void StartTension();

	void GetCurrentSpeed();

	virtual void Tick(float DeltaTime) override;

protected:

	virtual void WeaponShot() override;

	FTimeline TensionTimeLine;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Parameters")
	class UCurveFloat* SpeedOnTensionCurve;

	UPROPERTY(EditDefaultsOnly, Category = "Projectile | Parameters, meta = (ClampMin = 1.0f, UIMin = 1.0f)")
	float InitialSpeed = 1200.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Parameters, meta = (ClampMin = 1.0f, UIMin = 1.0f)")
	float MaxSpeed = 3000.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Parameters")
	float SpeedIncreasePerSecond = 150.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Arrow")
	TSubclassOf<class AGCProjectile> ProjectileClass;

	float TensionTime; //For TensionTime Curve

	bool bIsTension;
};

//������� ��� ����� � � ���� ������� �������� ��� ���� bIsAiming �� ��� ���������� � ����� ��������� ����������� �� ������������� � �����
