#include "BowWeaponItem.h"

#include "Components/Weapon/WeaponBarellComponent.h"
#include "Characters/GCBaseCharacter.h"
#include "Actors/Projectiles/GCProjectile.h"

ABowWeaponItem::ABowWeaponItem()
{
	PrimaryActorTick.bCanEverTick = true;
}

FTransform ABowWeaponItem::GetForeGripTransform() const
{
	return WeaponMesh->GetSocketTransform(SocketWeaponForeGrip);
}

void ABowWeaponItem::WeaponShot()
{
	bIsTension = false;

	checkf(GetOwner()->IsA<AGCBaseCharacter>(), TEXT("ABowWeaponItem::WeaponShot() only AGCBaseCharacter can be an owner of range weapon"));
	AGCBaseCharacter* CharacterOwner = StaticCast<AGCBaseCharacter*>(GetOwner());

	APlayerController* Controller = CharacterOwner->GetController<APlayerController>();
	if (!IsValid(Controller))
	{
		return;
	}

	FVector PlayerViewPoint;
	FRotator PlayerViewRotation;
	 
	Controller->GetPlayerViewPoint(PlayerViewPoint, PlayerViewRotation);
	
	//this one was added

		FVector SpawnArrowlocation = WeaponBarell->GetSocketLocation(SocketWeaponMuzzle);
		FVector ViewDirection = PlayerViewRotation.RotateVector(FVector::ForwardVector);

		AGCProjectile* ArrowProjectile = GetWorld()->SpawnActor<AGCProjectile>(ProjectileClass, SpawnArrowlocation, PlayerViewRotation);//FRotator::ZeroRotator);

		if (IsValid(ArrowProjectile))
		{
			GetCurrentSpeed();
			ArrowProjectile->SetOwner(GetOwner());
			ArrowProjectile->LaunchProjectile(ViewDirection, InitialSpeed); //��� � ������� �� ������������
			TensionTime = 0;
		}
}

void ABowWeaponItem::StartTension()
{
	bIsTension = true;
}

void ABowWeaponItem::GetCurrentSpeed()
{
	if (!bIsTension) //����� ��� �� ������ ���� ������ bIsTension?
	{
		InitialSpeed = SpeedOnTensionCurve->GetFloatValue(TensionTime);
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Current Tension time is %f"), TensionTime));
	}
	else
	{
		TensionTimeLine.Stop();
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Stop!"));
	}
}

void ABowWeaponItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (bIsTension)
	{
		TensionTime += DeltaTime;
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Current Tension time is %f"), TensionTime));
	}
}