#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "BasePlatform.generated.h"

class AOverlapBox;

UENUM()
enum class EPlatformBehavior : uint8
{
	OnDemand = 0,
	Loop
};

UCLASS()
class UELEARNING_API ABasePlatform : public AActor
{
	GENERATED_BODY()

public:
	ABasePlatform();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* PlatformMesh;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float OnDemandTimeStops = 5.0f;

	FTimeline PlatformTimeline;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCurveFloat* TimelineCurve;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Transient)
	FVector StartLocation;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, meta = (MakeEditWidget))
	FVector EndLocation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EPlatformBehavior PlatformBehavior = EPlatformBehavior::OnDemand;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	AOverlapBox* OverlapBox;

	UFUNCTION()
	void OnBoxTriggered(bool bIsOverlapped);

	UFUNCTION()
	void OnEndTimeline();

	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	void PlatformTimelineUpdate(float Alpha);
};