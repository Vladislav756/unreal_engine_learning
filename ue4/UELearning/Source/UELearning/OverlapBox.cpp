#include "OverlapBox.h"


AOverlapBox::AOverlapBox()
{
	//PrimaryActorTick.bCanEverTick = true; - only if needed to off
	//USceneComponent* DefaultOverlapBox = CreateDefaultSubobject<USceneComponent>(TEXT("OverlapBox"));
	//OverlapBox->GetScaledBoxExtent();
	//OverlapBox->SetCollisionProfileName(TEXT("OverlapComponent"));
	//OverlapBox->OnComponentBeginOverlap.AddDynamic(this, &AOverlapBox::OverlapBegin);
	//OverlapBox->OnComponentEndOverlap.AddDynamic(this, &AOverlapBox::OverlapEnd);

	OverlapBox = CreateDefaultSubobject<UBoxComponent>(FName("OverlapBoxComponent"));
	OverlapBox->SetCollisionProfileName(FName("PawnInteractionVolume"));
	SetRootComponent(OverlapBox);
}

void AOverlapBox::OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool Overlapped, const FHitResult& Overlap)
{
	APawn* OtherPawn = Cast<APawn>(OtherActor);
	if (!IsValid(OtherPawn)) 
	{
		return;
	}

	if (OnTriggerActivated.IsBound())
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Overlap work"));

		OnTriggerActivated.Broadcast(true);
	}
	
}

void AOverlapBox::OverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
 	APawn* OtherPawn = Cast<APawn>(OtherActor);
	if (!IsValid(OtherPawn))
	{
		return;
	}

	if (OnTriggerActivated.IsBound())
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Overlap stops"));

		OnTriggerActivated.Broadcast(false);
	}
}

void AOverlapBox::BeginPlay()
{
	Super::BeginPlay();
	
	OverlapBox->OnComponentBeginOverlap.AddDynamic(this, &AOverlapBox::OverlapBegin);
	OverlapBox->OnComponentEndOverlap.AddDynamic(this, &AOverlapBox::OverlapEnd);
}