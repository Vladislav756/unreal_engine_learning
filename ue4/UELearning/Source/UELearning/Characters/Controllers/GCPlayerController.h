#pragma once

#include "CoreMinimal.h"
#include "UObject/SoftObjectPtr.h"
#include "Engine/EngineBaseTypes.h"
#include "GameFramework/PlayerController.h"
#include "GCPlayerController.generated.h"

class AGCBaseCharacter;

UCLASS()
class UELEARNING_API AGCPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void SetPawn(APawn* InPawn) override;

protected:

	virtual void SetupInputComponent() override;

private:
	void MoveForward (float Value);
	void MoveRight(float Value);
	void Turn(float Value);
	void TurnAtRate(float Value);
	void LookUp(float Value);
	void LookUpAtRate(float Value);
	void Jump();
	void ChangeCrouchState();
	void StartSprint();
	void StopSprint();

	void PlayerStartFire();
	void PlayerStopFire();

	void StartAiming();
	void StopAiming();

	TSoftObjectPtr<AGCBaseCharacter> CachedBaseCharacter;
};
