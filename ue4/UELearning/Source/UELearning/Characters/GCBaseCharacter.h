#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/PlayerController.h"
#include "Characters/Components/CharacterComponents/CharacterAttributeComponent.h"
#include "GCBaseCharacter.generated.h"

class UCharacterAttributeComponent;
class UGCBaseCharacterMovementComponent;
class UCharacterEquipmentComponent;

UCLASS(Abstract, NotBlueprintable)
class UELEARNING_API AGCBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character | Aim")
	void OnStartAiming();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character | Aim")
	void OnStopAiming();

	AGCBaseCharacter(const FObjectInitializer& ObjectInitilizer);

	virtual void BeginPlay() override;

	virtual void MoveForward(float Value) {};
	virtual void MoveRight(float Value) {};
	virtual void Turn(float Value) {};
	virtual void TurnAtRate(float Value) {};
	virtual void LookUp(float Value) {};
	virtual void LookUpAtRate(float Value) {};

	virtual void ChangeCrouchState();
	virtual void StartSprint();
	virtual void StopSprint();

	void OnStartFire();
	void OnStopFire();

	void StartAiming();
	void StopAiming();

	void StartTension();
	void StopTension();

	bool IsAiming() const { return bIsAiming; }

	float GetAimingMovementSpeed() const;

	virtual void Tick(float DeltaTime) override;

	UCharacterAttributeComponent* GetCharacterAttributesComponent() const { return CharacterAttributesComponent; }
	UCharacterEquipmentComponent* GetCharacterEquipmentComponent() const { return CharacterEquipmentComponent; }
	UGCBaseCharacterMovementComponent* GetBaseCharacterMovementComponent() /*const*/ { return GCBaseCharacterMovementComponent; }

	UFUNCTION(BlueprintCallable)
	float GetHealthPercent() const { return CharacterAttributesComponent->GetHealthPercent();};

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Character | Movement")
	void OnSprintStart();

	UFUNCTION(BlueprintImplementableEvent, Category = "Character | Movement")
	void OnSprintEnd();

	virtual void OnDeath();

	virtual bool CanSprint();

	virtual void Falling() override;

	virtual void NotifyJumpApex() override;

	virtual void Landed(const FHitResult& Hit) override;

	virtual void OnStartAiming_Internal() { }
	virtual void OnStopAiming_Internal() { }

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Character | Controls")
	float BaseTurnRate = 45.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Character | Controls")
	float BaseLookUpRate = 45.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Character | Movement")
	float SprintSpeed = 800.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Stamina")
	float MaxStamina = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Stamina")
	float StaminaRestoreVelocity = 60.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Stamina")
	float SprintStaminaConsumptionVelocity = 50.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Components")
	UCharacterAttributeComponent* CharacterAttributesComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character | Attributes")
	class UCurveFloat* FallDamageCurve;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character | Animations")
	class UAnimMontage* OnDeathAnimMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Animations")
	UCharacterEquipmentComponent* CharacterEquipmentComponent;

	UGCBaseCharacterMovementComponent* GCBaseCharacterMovementComponent;

private:
	void TryChangeSprintState();

	void EnableRagdoll();

	bool bIsSprintRequested = false;

	float CurrentStamina = MaxStamina;

	bool bIsAiming = false;

	float CurrentAimingMovementSpeed;

	FTimerHandle DeathMontageTimer;

	FVector CurrentFallApex;
};