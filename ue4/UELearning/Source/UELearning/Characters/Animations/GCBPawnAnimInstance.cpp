#include "GCBPawnAnimInstance.h"
#include "Characters/GameCodeBasePawn.h"
#include "Components/MovementComponents/GCBasePMovementComponent.h"

void UGCBPawnAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
	checkf(TryGetPawnOwner()->IsA<AGameCodeBasePawn>(), TEXT("UGCBPawnAnimInstance::NativeBeginPlay() only GameCodeBasePawn can work with UGCBPawnAnimInstance"))
	CachedBasePawn = StaticCast<AGameCodeBasePawn*>(TryGetPawnOwner());

}


void UGCBPawnAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (!CachedBasePawn.IsValid())
	{
		return;
	}

	InputForward = CachedBasePawn->GetInputForward();
	InputRight = CachedBasePawn->GetInputRight();
	bIsInAir = CachedBasePawn->GetMovementComponent()->IsFalling();
} 
