
#include "AnimNotify_EnableRagdoll.h"
#include "../../../GameCodeTypes.h"

void UAnimNotify_EnableRagdoll::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	MeshComp->SetCollisionProfileName(CollisionRagdollProfile);
	MeshComp->SetSimulatePhysics(true);
}