#include "BowCharacterBaseAnimInstance.h"

#include "Actors/Equipment/Weapons/RangeWeaponItem.h"
#include "Characters/Components/CharacterComponents/CharacterEquipmentComponent.h"
#include "Characters/BowPlayerCharacter.h"
#include "Components/MovementComponents/GCBaseCharacterMovementComponent.h"


void UBowCharacterBaseAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
	checkf(TryGetPawnOwner()->IsA<ABowPlayerCharacter>(), TEXT("UBowCharacterBaseAnimInstance::NativeBeginPlay() can be used only with ABowPlayerCharacter"));
	CachedBaseCharacter = StaticCast<ABowPlayerCharacter*>(TryGetPawnOwner());
}

void UBowCharacterBaseAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (!CachedBaseCharacter.IsValid())
	{
		return;
	}
	
	bIsAiming = CachedBaseCharacter->IsAiming();

	UGCBaseCharacterMovementComponent* CharacterMovement = CachedBaseCharacter->GetBaseCharacterMovementComponent();
	Speed = CharacterMovement->Velocity.Size();
	bIsFalling = CharacterMovement->IsFalling();
	bIsCrouching = CharacterMovement->IsCrouching();
	bIsSprinting = CharacterMovement->IsSprinting();

	const UCharacterEquipmentComponent* CharacterEquipment = CachedBaseCharacter->GetCharacterEquipmentComponent();
	CurrentEquippedItem = CharacterEquipment->GetCurrentEquippedItemType();

	AimRotation = CachedBaseCharacter->GetBaseAimRotation();

//	AMachineryWeaponItem* CurrentRangeWeapon = Cast<AMachineryWeaponItem>(CharacterEquipment->GetCurrentRangeWeapon()); //this was changed from ARangeWeapon to AMachineryWeapon

	//if (IsValid(CurrentRangeWeapon))
	//{
	//	ForeGripSocketTransform = CurrentRangeWeapon->GetForeGripTransform();
	//}
}
