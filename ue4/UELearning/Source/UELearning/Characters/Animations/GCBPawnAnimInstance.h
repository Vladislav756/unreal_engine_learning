// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "GCBPawnAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class UELEARNING_API UGCBPawnAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public: 
	virtual void NativeBeginPlay() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

private:
	TWeakObjectPtr <class AGameCodeBasePawn> CachedBasePawn;

protected:

	UPROPERTY(BlueprintReadOnly, Transient, Category = "Base pawn animation instance")
	float InputForward;

	UPROPERTY(BlueprintReadOnly, Transient, Category = "Base pawn animation instance")
	float InputRight;

	UPROPERTY(BlueprintReadOnly, Transient, Category = "Base pawn animation instance")
	bool bIsInAir;
};
