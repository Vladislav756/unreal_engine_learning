#pragma once

#include "CoreMinimal.h"
#include "Characters/GCBaseCharacter.h"
#include "Components/MovementComponents/GCBaseCharacterMovementComponent.h"
#include "BowPlayerCharacter.generated.h"

UCLASS(Blueprintable)
class UELEARNING_API ABowPlayerCharacter : public AGCBaseCharacter
{
	GENERATED_BODY()
	
public:
	ABowPlayerCharacter(const FObjectInitializer& ObjectInitializer);

	 
	//APlayerCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void MoveForward(float Value) override;
	virtual void MoveRight(float Value) override;
	virtual void Turn(float Value) override;
	virtual void TurnAtRate(float Value) override;
	virtual void LookUp(float Value) override;
	virtual void LookUpAtRate(float Value) override; 

	virtual void OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;
	virtual void OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;

	virtual bool CanJumpInternal_Implementation() const override;
	virtual void OnJumped_Implementation() override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Camera")
	class UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Camera")
	class USpringArmComponent* SpringArmComponent;

	virtual void OnStartAiming_Internal() override;

	virtual void OnStopAiming_Internal() override;
};
