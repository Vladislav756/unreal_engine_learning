
#include "CharacterEquipmentComponent.h"

#include "Actors/Equipment/Weapons/RangeWeaponItem.h"
#include "Characters/GCBaseCharacter.h"
#include "GameCodeTypes.h"

EEquipableItemType UCharacterEquipmentComponent::GetCurrentEquippedItemType() const
{
	EEquipableItemType Result = EEquipableItemType::None;
	if (IsValid(CurrentEquippedWeapon))
	{
		Result = CurrentEquippedWeapon->GetItemType();
	}
	return Result;
}

void UCharacterEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();
	checkf(GetOwner()->IsA<AGCBaseCharacter>(), TEXT("UCharacterEquipmentComponent::BeginPlay can be used only with a BaseCharacter"));
	CashedBaseCharacter = StaticCast<AGCBaseCharacter*>(GetOwner());
	CreateLoadout();
}


void UCharacterEquipmentComponent::CreateLoadout()
{
	if (!IsValid(SideArmClass))
	{
		return;
	}
	CurrentEquippedWeapon = GetWorld()->SpawnActor<ARangeWeaponItem>(SideArmClass);

	if (CurrentEquippedWeapon->GetItemType() == EEquipableItemType::Bow)
	{
		CurrentEquippedWeapon->AttachToComponent(CashedBaseCharacter->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, SocketCharacterBow);
		CurrentEquippedWeapon->SetOwner(CashedBaseCharacter.Get());
	}
	else
	{
		CurrentEquippedWeapon->AttachToComponent(CashedBaseCharacter->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, SocketCharacterWeapon);
		CurrentEquippedWeapon->SetOwner(CashedBaseCharacter.Get());
	}
	
}