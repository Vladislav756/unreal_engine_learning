#include "GCBaseCharacter.h"

#include "Actors/Equipment/Weapons/BowWeaponItem.h"
#include "Actors/Equipment/Weapons/MachineryWeaponItem.h" //this one was added
#include "Components/CharacterComponents/CharacterAttributeComponent.h"
#include "Components/CharacterComponents/CharacterEquipmentComponent.h"
#include "Components/MovementComponents/GCBaseCharacterMovementComponent.h"
#include "GameCodeTypes.h"
#include "GameFramework/CharacterMovementComponent.h"

AGCBaseCharacter::AGCBaseCharacter(const FObjectInitializer& ObjectInitilizer)
	: Super(ObjectInitilizer.SetDefaultSubobjectClass<UGCBaseCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	GCBaseCharacterMovementComponent = StaticCast<UGCBaseCharacterMovementComponent*>(GetCharacterMovement());
	CharacterAttributesComponent = CreateDefaultSubobject<UCharacterAttributeComponent>(TEXT("CharacterAttributes"));
	CharacterEquipmentComponent = CreateDefaultSubobject<UCharacterEquipmentComponent>(TEXT("CharacterEquipment"));
	PrimaryActorTick.bCanEverTick = true;
}

void AGCBaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	CharacterAttributesComponent->OnDeathEvent.AddUObject(this, &AGCBaseCharacter::OnDeath);
}

void AGCBaseCharacter::ChangeCrouchState()
{
	if (GetCharacterMovement()->IsCrouching())
	{
		UnCrouch();
	}
	else
	{
		Crouch();
	}
}

void AGCBaseCharacter::StartSprint()
{
	bIsSprintRequested = true;

	if (bIsCrouched)
	{
		UnCrouch();
	}
}

void AGCBaseCharacter::StopSprint()
{
	bIsSprintRequested = false;	
	GCBaseCharacterMovementComponent->StopSprint();
}
 
void AGCBaseCharacter::OnStartFire()
{
	ARangeWeaponItem* CurrentRangeWeapon = CharacterEquipmentComponent->GetCurrentRangeWeapon();
		if (IsValid(CurrentRangeWeapon))
		{
			EEquipableItemType ItemType = CurrentRangeWeapon->GetItemType();
			if (ItemType == EEquipableItemType::Pistol || ItemType == EEquipableItemType::Rifle)
			{
				CurrentRangeWeapon->WeaponShot();
			}
			if (ItemType == EEquipableItemType::Bow)
			{
				ABowWeaponItem* BowWeapon = Cast<ABowWeaponItem>(CurrentRangeWeapon);
				if (IsValid(BowWeapon)) 
				{
					BowWeapon->StartTension();
				}
			}
		}
}

void AGCBaseCharacter::OnStopFire()
{
	ARangeWeaponItem* CurrentRangeWeapon = CharacterEquipmentComponent->GetCurrentRangeWeapon();
	if (IsValid(CurrentRangeWeapon))
	{
		EEquipableItemType ItemType = CurrentRangeWeapon->GetItemType();
		if (ItemType == EEquipableItemType::Pistol || ItemType == EEquipableItemType::Rifle)
		{
			CurrentRangeWeapon->OnWeaponStopFire();
		}	
		if (ItemType == EEquipableItemType::Bow)
		{
			CurrentRangeWeapon->WeaponShot();
		}
	}
}

void AGCBaseCharacter::StartAiming()
{
	ARangeWeaponItem* CurrentRangeWeapon = GetCharacterEquipmentComponent()->GetCurrentRangeWeapon();
	if (!IsValid(CurrentRangeWeapon))
	{
		return;
	}

	bIsAiming = true;
	CurrentAimingMovementSpeed = CurrentRangeWeapon->GetAimMovementMaxSpeed();
	CurrentRangeWeapon->StartAim();
	OnStartAiming_Implementation();
}

void AGCBaseCharacter::StopAiming()
{
	if (!bIsAiming)
	{
		return;
	}

	ARangeWeaponItem* CurrentRangeWeapon = GetCharacterEquipmentComponent()->GetCurrentRangeWeapon();
	if (IsValid(CurrentRangeWeapon))
		{
			CurrentRangeWeapon->StopAim();
		}

	bIsAiming = false;

	OnStopAiming_Implementation();
}

float AGCBaseCharacter::GetAimingMovementSpeed() const
{
	return CurrentAimingMovementSpeed;
}

void AGCBaseCharacter::OnStartAiming_Implementation()
{
	OnStartAiming_Internal();
}

void AGCBaseCharacter::OnStopAiming_Implementation()
{
	OnStopAiming_Internal();
}

void AGCBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TryChangeSprintState();
	
	/*if (CurrentStamina != MaxStamina)
	{
		GEngine->AddOnScreenDebugMessage(1, 1.0f, FColor::Yellow, FString::Printf(TEXT("Stamina: %.2f"), CurrentStamina));
	}*/

	if (!GCBaseCharacterMovementComponent->IsOutOfStamina() && CurrentStamina == 0)
	{
		GCBaseCharacterMovementComponent->SetIsOutOfStamina(true);
	}

	if (GCBaseCharacterMovementComponent->IsOutOfStamina() && CurrentStamina == MaxStamina)
	{
		GCBaseCharacterMovementComponent->SetIsOutOfStamina(false);
	}
}

void AGCBaseCharacter::OnDeath()
{
	float Duration = PlayAnimMontage(OnDeathAnimMontage);
	if (FMath::IsNearlyEqual(Duration, 0.0f))
	{
		EnableRagdoll();
	}
	GetCharacterMovement()->DisableMovement();
}

bool AGCBaseCharacter::CanSprint()
{
	if (GCBaseCharacterMovementComponent->IsOutOfStamina())
	{
		return false;
	}

	return true;
}

void AGCBaseCharacter::Falling()
{
	GetBaseCharacterMovementComponent()->bNotifyApex = true;
}

void AGCBaseCharacter::NotifyJumpApex()
{
	Super::NotifyJumpApex();
	CurrentFallApex = GetActorLocation();
}

void AGCBaseCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	const float FallHeight = (CurrentFallApex - GetActorLocation()).Z * 0.01f;
	if (IsValid(FallDamageCurve))
	{
		const float DamageAmount = FallDamageCurve->GetFloatValue(FallHeight);
		if (IsValid(Hit.Actor.Get()))
		{
			TakeDamage(DamageAmount, FDamageEvent(), GetController(), Hit.Actor.Get());
		}
	}
}

void AGCBaseCharacter::TryChangeSprintState()
{
	if (bIsSprintRequested && !GCBaseCharacterMovementComponent->IsSprinting() && CanSprint() && !GCBaseCharacterMovementComponent->IsOutOfStamina())
	{
		GCBaseCharacterMovementComponent->StartSprint();
		OnSprintStart();
	}

	if (GCBaseCharacterMovementComponent->IsSprinting() && (!bIsSprintRequested || GCBaseCharacterMovementComponent->IsOutOfStamina() || !CanSprint()))
	{
		GCBaseCharacterMovementComponent->StopSprint();
		OnSprintEnd();
	}

	if (GCBaseCharacterMovementComponent->IsSprinting())
	{
		float DeltaTime = GetWorld()->GetDeltaSeconds();
		CurrentStamina -= SprintStaminaConsumptionVelocity * DeltaTime;
		CurrentStamina = FMath::Clamp(CurrentStamina, 0.0f, MaxStamina);
	}

	if (!GCBaseCharacterMovementComponent->IsSprinting())
	{
		float DeltaTime = GetWorld()->GetDeltaSeconds();
		CurrentStamina += StaminaRestoreVelocity * DeltaTime;
		CurrentStamina = FMath::Clamp(CurrentStamina, 0.0f, MaxStamina);
	}
}

void AGCBaseCharacter::EnableRagdoll()
{
	GetMesh()->SetCollisionProfileName(CollisionRagdollProfile);
	GetMesh()->SetSimulatePhysics(true);
}
